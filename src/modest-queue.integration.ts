import 'reflect-metadata'
import { ModestQueue } from './modest-queue'
import Redis from 'ioredis'
import * as faker from 'faker'
import { sleep } from './utility/sleep'
import * as uuid from 'uuid'
import { DelayedStatus, QueueOptions } from './contracts'
import { Milliseconds, RetryStrategy } from './retry-strategy'

class SimpleRetryStrategy implements RetryStrategy {
  calculateRetryDelay(currentAttempt: number): Milliseconds {
    return currentAttempt * 1000
  }
}

function generateSut(options?: Partial<QueueOptions>): ModestQueue {
  return new ModestQueue({
    queueName: uuid.v4(),
    connectionString: 'redis://127.0.0.1:6379',
    visibilityTimeout: 1000,
    maxAttempts: 5,
    withScheduler: false,
    withDelayedScheduler: false,
    retryStrategy: new SimpleRetryStrategy(),
    ...options
  })
}

describe('modest-queue', () => {
  describe.each([
    ['with a redis connection', true],
    ['without a redis connection', false]
  ])(`when initialized %s`, (_, withConnection: boolean) => {
    describe.each([
      ['scheduler enabled and the delayed scheduler enabled', true, true],
      ['scheduler enabled and the delayed scheduler disabled', true, false],
      ['scheduler disabled and the delayed scheduler enabled', false, true],
      ['scheduler disabled and the delayed scheduler disabled', false, true]
    ])(
      `when initialized with the %s`,
      (_, schedulerEnabled: boolean, delayedSchedulerEnabled: boolean) => {
        it('starts up and shuts down correctly', async () => {
          // Create a redis connection that might be supplied in initialize
          const redisConnection = new Redis('redis://127.0.0.1:6379')
          const redisConnectionQuitSpy = jest.spyOn(redisConnection, 'quit')
          const sut = generateSut({
            withScheduler: schedulerEnabled,
            withDelayedScheduler: delayedSchedulerEnabled,
            connection: withConnection ? redisConnection : undefined,
            connectionString: withConnection
              ? undefined
              : 'redis://127.0.0.1:6379'
          })

          const pollForInflightSpy = jest.spyOn<any, string>(
            sut,
            'pollForInflight'
          )
          const pollNextInflightMessageSpy = jest.spyOn<any, string>(
            sut,
            'pollNextInflightMessage'
          )

          const pollForDelayedSpy = jest.spyOn<any, string>(
            sut,
            'pollForDelayed'
          )
          const pollNextDelayedMessageSpy = jest.spyOn<any, string>(
            sut,
            'pollNextDelayedMessage'
          )

          await sut.initialize()
          const internalRedisConnectionSpy = jest.spyOn<any, string>(
            sut['connection'],
            'quit'
          )

          // if connection was supplied it should update the boolean flag to indicate so
          expect(sut['connectionPassedIn']).toEqual(withConnection)

          // Check the inflight loop has started if the scheduler is enabled
          expect(pollForInflightSpy).toHaveBeenCalledTimes(
            schedulerEnabled ? 1 : 0
          )

          if (schedulerEnabled) {
            expect(pollNextInflightMessageSpy).toBeCalled()
          } else {
            expect(pollNextInflightMessageSpy).not.toBeCalled()
          }

          // Check the delayed loop has started if the delayed scheduler is enabled
          expect(pollForDelayedSpy).toHaveBeenCalledTimes(
            delayedSchedulerEnabled ? 1 : 0
          )

          if (delayedSchedulerEnabled) {
            expect(pollNextDelayedMessageSpy).toBeCalled()
          } else {
            expect(pollNextDelayedMessageSpy).not.toBeCalled()
          }

          await sut.dispose()
          expect(redisConnectionQuitSpy).not.toHaveBeenCalled()
          expect(internalRedisConnectionSpy).toHaveBeenCalledTimes(
            withConnection ? 0 : 1
          )
          await redisConnection.quit()
        })
      }
    )
  })

  describe('publish and pollForMessage', () => {
    let sut: ModestQueue
    beforeEach(async () => {
      sut = generateSut({ withScheduler: true })
      await sut.initialize()
    })
    afterEach(async () => {
      await sut.destroyQueue()
      await sut.dispose()
    })
    it('when polling and there are no messages on the queue returns undefined', async () => {
      expect(await sut.pollForMessage()).toBeUndefined()
    })
    it('when polling and there is a message on the queue it returns it and places it on inflight', async () => {
      const fakeMessage = faker.random.words()
      await sut.publish(fakeMessage)
      const message = await sut.pollForMessage()
      expect(message).toMatchObject({
        message: fakeMessage,
        metadata: {
          attemptsLeft: 4,
          currentAttempt: 1,
          maxAttempts: 5
        }
      })
      expect(await sut.queueStats()).toEqual({
        queue: 0,
        dlq: 0,
        delayed: 0,
        inflight: 1
      })
    })
    it('failed messages respect the retry delay', async () => {
      const fakeMessage = faker.random.words()
      await sut.publish(fakeMessage)
      const message = await sut.pollForMessage()
      // fail the message
      await sut.messageFailed(message!)
      // poll immediately, there should be no message
      const emptyMessage = await sut.pollForMessage()
      expect(emptyMessage).toBeUndefined()
      // wait for the delay time - should be 1 second:
      await sleep(1100)
      const messageAbleToBeRetried = await sut.pollForMessage()
      expect(messageAbleToBeRetried).toMatchObject({
        message: fakeMessage,
        metadata: {
          attemptsLeft: 3,
          currentAttempt: 2,
          maxAttempts: 5
        }
      })
      // fail it again, should be retryable after 2 seconds
      await sut.messageFailed(messageAbleToBeRetried!)
      await sleep(1100)

      // shouldn't be a message to process as retryStrategyDelay hasn't been exceeded
      const emptyMessageAgain = await sut.pollForMessage()
      expect(emptyMessageAgain).toBeUndefined()
      await sleep(1100)

      // as another second has passed, it should now be available for another attempt
      const messageAbleToBeRetriedAgain = await sut.pollForMessage()
      expect(messageAbleToBeRetriedAgain).toMatchObject({
        message: fakeMessage,
        metadata: {
          attemptsLeft: 2,
          currentAttempt: 3,
          maxAttempts: 5
        }
      })
    })
  })

  describe('publish and pollForMessage with delayed messages', () => {
    let sut: ModestQueue
    let message = faker.random.words()
    beforeEach(async () => {
      sut = generateSut({ withScheduler: false, withDelayedScheduler: true })
      await sut.initialize()
    })
    afterEach(async () => {
      await sut.destroyQueue()
      await sut.dispose()
    })
    it('when publishing a message with a delay it must wait for the delay time to pass before able to pull the message', async () => {
      await sut.publish(message, { delay: 2000 })
      expect(await sut.pollForMessage()).toBeUndefined()
      await sleep(2500)
      const polledMessage = await sut.pollForMessage()
      expect(polledMessage).toMatchObject({
        message,
        metadata: {
          attemptsLeft: 4,
          currentAttempt: 1,
          maxAttempts: 5
        }
      })
      expect(await sut.queueStats()).toEqual({
        queue: 0,
        dlq: 0,
        delayed: 0,
        inflight: 1
      })
    })
  })

  describe('publish and pollForMessage with priorities', () => {
    let sut: ModestQueue
    let message = faker.random.words()
    beforeEach(async () => {
      sut = generateSut({ withScheduler: false, withDelayedScheduler: true })
      await sut.initialize()
    })
    afterEach(async () => {
      await sut.destroyQueue()
      await sut.dispose()
    })
    it('when publishing a message with a delay it must wait for the delay time to pass before able to pull the message', async () => {
      // low priority
      await sut.publish(message, { priority: 25 })
      // medium priority message (default is 50)
      await sut.publish(message)
      // high priority message
      await sut.publish(message, { priority: 75 })

      const highPriorityMessage = await sut.pollForMessage()
      const mediumPriorityMessage = await sut.pollForMessage()
      const lowPriorityMessage = await sut.pollForMessage()

      expect(highPriorityMessage).toMatchObject({
        message,
        metadata: {
          attemptsLeft: 4,
          currentAttempt: 1,
          maxAttempts: 5,
          priority: 75
        }
      })
      expect(mediumPriorityMessage).toMatchObject({
        message,
        metadata: {
          attemptsLeft: 4,
          currentAttempt: 1,
          maxAttempts: 5,
          priority: 50
        }
      })
      expect(lowPriorityMessage).toMatchObject({
        message,
        metadata: {
          attemptsLeft: 4,
          currentAttempt: 1,
          maxAttempts: 5,
          priority: 25
        }
      })
      expect(await sut.queueStats()).toEqual({
        queue: 0,
        dlq: 0,
        delayed: 0,
        inflight: 3
      })
    })
  })

  describe('queueStats', () => {
    const sut = generateSut()
    const testMessage = faker.random.words()
    describe('when 5 messages are published and one is in flight, one was was successful, one was sent to the DLQ and one was delayed', () => {
      beforeEach(async () => {
        await sut.initialize()
        await sut.destroyQueue()
        // Publish 4 test messages
        await sut.publish(testMessage)
        await sut.publish(testMessage, { delay: 10000 })
        await sut.publish(testMessage)
        await sut.publish(testMessage)
        await sut.publish(testMessage)
        // poll and place first on DLQ
        const messageToSendToDLQ = await sut.pollForMessage()
        await sut.messageFailed(messageToSendToDLQ!, true)
        // have one land in inflight and not be handled
        await sut.pollForMessage()
        // have one be successful so not left in any queue
        const pollForSuccessFulMessage = await sut.pollForMessage()
        await sut.messageSucceeded(pollForSuccessFulMessage!)
      })
      afterEach(async () => {
        await sut.destroyQueue()
        await sut.dispose()
      })
      it('the queueStats correctly display this', async () => {
        const queueStats = await sut.queueStats()
        expect(queueStats).toEqual({
          queue: 1,
          dlq: 1,
          inflight: 1,
          delayed: 1
        })
      })
    })
  })

  describe('Happy Path', () => {
    const sut = generateSut({ maxAttempts: 2 })

    const testMessage = faker.random.words()
    beforeEach(async () => {
      await sut.initialize()
    })
    afterEach(async () => {
      await sut.destroyQueue()
      await sut.dispose()
    })
    it('Should traverse from the queues correctly', async () => {
      // Publish the test Message
      await sut.publish(testMessage)
      // Prove that despite the message being published, there are no messages in flight
      const noMessageInFlight = await sut['pollNextInflightMessage']()
      expect(noMessageInFlight).toEqual(DelayedStatus.NoMessageFound)
      // Poll for it - imitate a worker that stalls and doesn't report the message succeeded
      await sut.pollForMessage()
      // prove that whilst the message is being worked on, its in the inflight queue but is too early to be retried
      const messageTooEarly = await sut['pollNextInflightMessage']()
      expect(messageTooEarly).toEqual(DelayedStatus.PolledMessageTooEarly)
      // wait for it to have well surpassed the 2 second time out period
      await sleep(2100)
      // poll the inflight message, the visibility timeout has been exceeded, the message needs to be retried
      const messagePlacedBackOnQueue = await sut['pollNextInflightMessage']()
      expect(messagePlacedBackOnQueue).toEqual(
        DelayedStatus.MessagePlacedBackOnQueue
      )
      // poll for it and check its values make sense
      const message = await sut.pollForMessage()
      expect(message).toMatchObject({
        message: testMessage,
        metadata: {
          attemptsLeft: 0,
          currentAttempt: 2,
          maxAttempts: 2
        }
      })
      // wait again for its inflight to expire
      await sleep(2100)
      // check inflight, should now place in the DLQ
      await sut['pollNextInflightMessage']()
      // prove that its not in the regular queue
      expect(await sut.pollForMessage()).toBeUndefined()
      const replayCount = await sut.replayDLQ(1)
      expect(replayCount).toEqual(1)
      const messageReplayedFromDLQ = await sut.pollForMessage()
      expect(messageReplayedFromDLQ).toMatchObject({
        message: testMessage,
        metadata: {
          attemptsLeft: 1,
          currentAttempt: 3,
          maxAttempts: 2
        }
      })
      await sut.messageSucceeded(messageReplayedFromDLQ!)
      const queueStats = await sut.queueStats()
      expect(queueStats).toEqual({ queue: 0, dlq: 0, inflight: 0, delayed: 0 })
    })
  })

  describe('Benchmark', () => {
    const sut = generateSut()
    const testMessage = faker.random.words()
    beforeEach(async () => {
      await sut.initialize()
    })
    afterEach(async () => {
      await sut.destroyQueue()
      await sut.dispose()
    })
    it('Publishes and consumes 1000 messages', async () => {
      const startTime = Date.now()
      await Promise.all(
        new Array(1000).fill(undefined).map(_ => sut.publish(testMessage))
      )
      const publishFinishedTime = Date.now()
      const queueStatsPostPublish = await sut.queueStats()
      expect(queueStatsPostPublish).toEqual({
        queue: 1000,
        dlq: 0,
        inflight: 0,
        delayed: 0
      })
      await Promise.all(
        new Array(1000).fill(undefined).map(async _ => {
          const message = await sut.pollForMessage()
          await sut.messageSucceeded(message!)
        })
      )
      const pollingFinishedTime = Date.now()
      const queueStatsPostPolling = await sut.queueStats()
      expect(queueStatsPostPolling).toEqual({
        queue: 0,
        dlq: 0,
        inflight: 0,
        delayed: 0
      })
      console.log(
        `it took ${
          (publishFinishedTime - startTime) / 1000
        } seconds to publish 1000 messages`
      )
      console.log(
        `it took ${
          (pollingFinishedTime - publishFinishedTime) / 1000
        } seconds to poll 1000 messages`
      )
    })
  })
})
