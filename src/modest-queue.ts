import Redis from 'ioredis'
import * as uuid from 'uuid'
import { serialize, deserialize } from 'class-transformer'
import {
  Connection,
  DelayedStatus,
  Message,
  MessageMetaData,
  PassedInConnection,
  PassedInConnectionString,
  PublishOptions,
  QueueOptions,
  QueueStats
} from './contracts'
import { sleep } from './utility'
import { DefaultRetryStrategy, RetryStrategy } from './retry-strategy'

/**
 * A very simple queue based on the advice here:
 * https://redis.com/ebook/part-2-core-concepts/chapter-6-application-components-in-redis/6-4-task-queues/6-4-1-first-in-first-out-queues/
 * It utilizes a simple Redis list for the Queue - pushing messages to the right and processing messages by popping them from the left
 *
 * When a message is popped off the queue, it is persisted to an ordered list via ZADD with its score being the current unix time + the configured visibility timeout.
 * This Ordered List is polled by popping the messages off the list and checking whether their score is later than the current unix timestamp.
 * If it is, then that message has likely failed, as the caller that processes the message failed to notify us that it had succeeded or failed.
 *
 * If a message is handled successfully the caller must call messageSucceeded to correctly remove the message from the ordered list.
 * If the message has failed, the caller should call the messageFailed method to requeue it for another attempt
 *
 */
export class ModestQueue {
  private queueName: string
  private inflightSet: string
  private delayedSet: string
  private deadLetterQueue: string
  private visibilityTimeout: number
  private maxAttempts: number

  private connection: Connection
  private connectionString: string

  private initialized: boolean
  private withScheduler: boolean
  private withDelayedScheduler: boolean

  private connectionPassedIn: boolean
  private retryStrategy: RetryStrategy
  private retryStrategyDelayedSet: string

  constructor(options: QueueOptions) {
    const prefix = 'modest:queue'
    const dlq = 'dlq'
    const inflight = 'inflight'
    const delayed = 'delayed'
    const retryStrategyDelay = 'retry-strategy-delay'

    const {
      queueName,
      visibilityTimeout,
      maxAttempts,
      withScheduler,
      withDelayedScheduler,
      retryStrategy
    } = options

    this.queueName = `${prefix}:${queueName}`
    this.inflightSet = `${this.queueName}:${inflight}`
    this.delayedSet = `${this.queueName}:${delayed}`
    this.deadLetterQueue = `${this.queueName}:${dlq}`
    this.retryStrategyDelayedSet = `${this.queueName}:${retryStrategyDelay}`

    if ((options as PassedInConnectionString).connectionString) {
      this.connectionString = (
        options as PassedInConnectionString
      ).connectionString
      this.connectionPassedIn = false
    } else {
      this.connection = (options as PassedInConnection).connection
      this.connectionPassedIn = true
    }

    this.visibilityTimeout = visibilityTimeout ?? 30000
    this.maxAttempts = maxAttempts ?? 10
    this.withScheduler = withScheduler ?? true
    this.withDelayedScheduler = withDelayedScheduler ?? true
    this.retryStrategy = retryStrategy ?? new DefaultRetryStrategy()
  }

  /**
   * Creates the redis connection (if not supplied in the constructor) and begins polling
   * for inflight messages that may need to be retried if @see withScheduler is set to true.
   */
  public async initialize() {
    if (this.initialized) {
      console.error('already initialized')
      return
    }
    if (!this.connectionPassedIn) {
      this.connection = new Redis(this.connectionString)
    }

    this.initialized = true
    if (this.withScheduler) {
      this.pollForInflight()
      this.pollForDelayedRetry()
    }
    if (this.withDelayedScheduler) {
      this.pollForDelayed()
    }
  }

  /**
   * correctly disposes the redis connection and
   * stops polling for inflight messages
   *
   * If you supply a redis connection in the constructor, it is up to you to disconnect it!
   * Its the private variable 'connection' and you can call 'quit' on that.
   */
  public async dispose() {
    if (!this.initialized) {
      console.error('already disposed')
      return
    }
    this.initialized = false
    if (!this.connectionPassedIn) {
      await this.connection.quit()
    }
  }

  /**
   * Publishes the message to the queue
   * @param message the serialized message you wish to push to the queue
   * @param publishOptions per message options to append.
   */
  async publish(message: string, publishOptions?: PublishOptions) {
    const msg = this.createMessage(message, publishOptions)
    const serializedMessage = serialize(msg)
    if (publishOptions?.delay !== undefined) {
      if (publishOptions.delay <= 0) {
        throw new Error('Must choose a delay time that is greater than 0')
      }
      // Push to delayed queue
      await this.connection.zadd(
        this.delayedSet,
        Date.now() + publishOptions.delay,
        serializedMessage
      )
    } else {
      // Push to regular queue
      await this.connection.zadd(
        this.queueName,
        msg.metadata.score,
        serializedMessage
      )
    }
  }

  /**
   * Pops a message from the queue if one exists, returns undefined if it doesn't
   * Returns your serialized message wrapped in the @see Message class.
   * Once you have successfully handled the message, be sure to call @this messageSucceeded
   * so that it wont be automatically requeued after @see visabilityTimeout ms
   * If your worker fails to handle the message, call @see messageFailed
   */
  async pollForMessage(): Promise<Message | undefined> {
    const [serializedMessage, score] = await this.connection.zpopmin(
      this.queueName
    )
    if (!serializedMessage) {
      return undefined
    }
    const message = deserialize(Message, serializedMessage)
    // This attempts left is first set by the queue worker that consumes the message, not when the message is created
    if (message.metadata.attemptsLeft === undefined) {
      message.metadata.attemptsLeft = this.maxAttempts
      message.metadata.maxAttempts = this.maxAttempts
    }

    message.metadata.attemptsLeft--
    if (message.metadata.attemptsLeft < 0) {
      await this.connection.rpush(this.deadLetterQueue, serialize(message))
      // Loop
      return this.pollForMessage()
    }

    message.metadata.processedDate = new Date()
    message.metadata.currentAttempt++

    // Persist to ordered set https://redis.com/ebook/part-2-core-concepts/chapter-6-application-components-in-redis/6-4-task-queues/6-4-2-delayed-tasks/
    await this.connection.zadd(
      this.inflightSet,
      message.metadata.processedDate.getTime() + this.visibilityTimeout,
      serialize(message)
    )
    return message
  }

  /**
   * If your message fails, be sure to call this to have the message requeued.
   * @param message The message that failed, that you want to requeue.
   * @param moveToDeadLetterQueue Set to true if you would like to move this to the DLQ and not retry
   */
  async messageFailed(
    message: Message,
    moveToDeadLetterQueue = false
  ): Promise<void> {
    const serializedMessage = serialize(message)

    const attemptRemoveMessage = await this.connection.zrem(
      this.inflightSet,
      serializedMessage
    )

    if (attemptRemoveMessage === 1) {
      // message was removed, which means it wasn't automatically requeued
      // push to be retried
      moveToDeadLetterQueue
        ? await this.connection.rpush(this.deadLetterQueue, serializedMessage)
        : await this.connection.zadd(
            this.retryStrategyDelayedSet,
            this.calculateRetryAttemptScore(message.metadata.currentAttempt),
            serializedMessage
          )
    }
  }

  /**
   * This function must be called once your worker has handled your message. If you don't call this, we will assume that the
   * message failed and after the @see visibilityTimeout it will be placed back on the queue to be retried
   * @param message The message that succeeded
   */
  async messageSucceeded(message: Message): Promise<void> {
    await this.connection.zrem(this.inflightSet, serialize(message))
  }

  /**
   * returns the amount of messages that exist on each of the queues, and the amount that are currently inflight.
   */
  async queueStats(): Promise<QueueStats> {
    const dlqCountPromise = this.connection.llen(this.deadLetterQueue)
    const queueCountPromise = this.connection.zcard(this.queueName)
    const inflightCountPromise = this.connection.zcard(this.inflightSet)
    const delayedCountPromise = this.connection.zcard(this.delayedSet)

    return {
      queue: await queueCountPromise,
      dlq: await dlqCountPromise,
      inflight: await inflightCountPromise,
      delayed: await delayedCountPromise
    }
  }

  /**
   *
   * @param max the maximum amount of messages you wish to be moved back to the queue to be retried again
   * defaults to 10
   * @returns the amount of messages successfully replayed to the queue
   */
  async replayDLQ(max = 10): Promise<number> {
    let replayCount = 0
    for (let i = 0; i < max; i++) {
      const serializedMessage = await this.connection.lpop(this.deadLetterQueue)
      if (serializedMessage) {
        const message = deserialize(Message, serializedMessage)

        // need to ensure that there's another this.max attempts allotted before pushing back on the queue
        message.metadata.attemptsLeft = undefined
        await this.connection.zadd(
          this.queueName,
          message.metadata.score,
          serialize(message)
        )
        replayCount++
      } else {
        break
      }
    }
    return replayCount
  }
  /**
   * WARNING: This will delete all queued messages, the status of any that are currently
   * in progress and any that are in the DLQ. This is useful for testing, and if you are trying
   * to clean up your redis instance and no longer want to use this library!
   *
   * This method also comes with no guarantees! backup before you play with this!
   */
  async destroyQueue(): Promise<void> {
    await Promise.all([
      this.connection.del(this.deadLetterQueue),
      this.connection.del(this.inflightSet),
      this.connection.del(this.queueName),
      this.connection.del(this.delayedSet),
      this.connection.del(this.retryStrategyDelayedSet)
    ])
  }

  /**
   * Wraps the clients serialized message in the Message class with useful metadata
   * the token ensures that serialized payload is always unique and that there aren't collisions
   * when placing these messages into redis
   * @param message the serialized payload to place on the queue
   * @param publishOptions additional options when the message was published
   */
  private createMessage(
    message: string,
    publishOptions?: PublishOptions
  ): Message {
    // set the default priority to somewhere in the middle
    const priority = publishOptions?.priority ?? 50
    const publishDate = new Date()
    return new Message(
      message,
      new MessageMetaData(
        uuid.v4(),
        publishDate,
        0,
        priority,
        this.calculateQueueScore(
          priority,
          publishOptions?.delay !== undefined
            ? new Date(publishDate.getTime() + publishOptions.delay)
            : publishDate
        ),
        publishOptions?.delay
      )
    )
  }

  /**
   * When a message fails, it will use the this.retryStrategy to delay the message from being
   * processed. To achieve this, it is placed in a zset with a score that is milliseconds into the future
   * of when it can be promoted back to the regular queue.
   */
   private async pollForDelayedRetry() {
    while (this.initialized && this.withScheduler) {
      const messageReturnedToQueue = await this.pollNextDelayedRetryMessage()
      if (messageReturnedToQueue !== DelayedStatus.MessagePlacedBackOnQueue) {
        await sleep(100)
      }
    }
  }

  /**
   * Checks any messages that are currently being worked on by a users worker, haven't timed out.
   * If they have, they are placed back on the queue
   */
  private async pollNextDelayedRetryMessage(): Promise<DelayedStatus> {
    // This wraps pollNextDelayedMessageFromQueue for making testing simpler
    return this.pollNextDelayedMessageFromQueue(
      this.retryStrategyDelayedSet,
      this.queueName
    )
  }

  /**
   * Checks any messages that are currently being worked on by a users worker, haven't timed out.
   * If they have, they are placed back on the queue
   */
  private async pollForInflight() {
    while (this.initialized && this.withScheduler) {
      const messageReturnedToQueue = await this.pollNextInflightMessage()
      if (messageReturnedToQueue !== DelayedStatus.MessagePlacedBackOnQueue) {
        await sleep(100)
      }
    }
  }

  /**
   * Checks any messages that are currently being worked on by a users worker, haven't timed out.
   * If they have, they are placed back on the queue
   */
  private async pollNextInflightMessage(): Promise<DelayedStatus> {
    // This wraps pollNextDelayedMessageFromQueue for making testing simpler
    return this.pollNextDelayedMessageFromQueue(
      this.inflightSet,
      this.queueName
    )
  }

  /**
   * Checks if any delayed messages are ready to be promoted back into the queue to be processed
   */
  private async pollForDelayed() {
    while (this.initialized && this.withDelayedScheduler) {
      const messageReturnedToQueue = await this.pollNextDelayedMessage()
      if (messageReturnedToQueue !== DelayedStatus.MessagePlacedBackOnQueue) {
        await sleep(100)
      }
    }
  }
  /**
   * Checks if any delayed messages are ready to be promoted back into the queue to be processed
   */
  private async pollNextDelayedMessage(): Promise<DelayedStatus> {
    // This wraps pollNextDelayedMessageFromQueue for making testing simpler
    return this.pollNextDelayedMessageFromQueue(this.delayedSet, this.queueName)
  }

  /**
   *
   * @param queueWithDelayedMessages - The queue that contains messages that are only ready to be promoted to @see queueToPromoteTo when the time has surpassed their score
   * @param queueToPromoteTo - Once the current unix timestamp exceeds or matches the messages score it is placed on this queue, ready to be picked up by workers
   * @returns
   */
  private async pollNextDelayedMessageFromQueue(
    queueWithDelayedMessages: string,
    queueToPromoteTo: string
  ): Promise<DelayedStatus> {
    const [item, score] = await this.connection.zpopmin(
      queueWithDelayedMessages
    )
    if (!item) {
      return DelayedStatus.NoMessageFound
    }
    if (Number.parseInt(score, 10) >= Date.now()) {
      // Too early - pop back onto the set
      await this.connection.zadd(
        queueWithDelayedMessages,
        Number.parseInt(score, 10),
        item
      )
      return DelayedStatus.PolledMessageTooEarly
    }
    const message = deserialize(Message, item)
    // not too early, push back onto the queue
    await this.connection.zadd(queueToPromoteTo, message.metadata.score, item)
    return DelayedStatus.MessagePlacedBackOnQueue
  }
  /**
   * A priority queue can be achieved by manipulating an ordered set.
   * If we take the current time in milliseconds and offset it by an amount,
   * in this case, 4 weeks in milliseconds, we can create windows for messages.
   * This will ensure two things:
   * 1. Their FIFO order in their priority subset is respected
   * 2. 4 weeks is a long time for messages to have not been serviced - if you fail to process a message in 4 weeks, it will bleed into other priorities
   *
   * @param priority The published message priority
   * @param date the supplied Date that you want to offset by the priority time
   */
  private calculateQueueScore(priority: number, date: Date): number {
    // 1000 * 60 * 60 * 24 * 7 * 4
    const millisecondsInFourWeeks = 2419200000
    return priority * -1 * millisecondsInFourWeeks + date.getTime()
  }

  /**
   * Calculates the score for the delayedRetryQueue so that it promotes the message back
   * to the queue after the right amount of time has expired
   */
  private calculateRetryAttemptScore(currentAttempt: number): number {
    return new Date().getTime() + this.retryStrategy.calculateRetryDelay(currentAttempt)
  }
}
