export interface PublishOptions {
  /**
   * Time in milliseconds before this message should become visible
   * on the queue.
   */
  delay?: number
  /**
   * Number between 0 and 100
   * 100 is the highest priority, 0 is the lowest
   */
  priority?: number
}
