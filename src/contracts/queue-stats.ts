export interface QueueStats {
  /**
   * The amount of messages on the queue
   */
  queue: number
  /**
   * The amount of messages on the dead letter queue
   */
  dlq: number
  /**
   * The amount of messages that are currently being processed by workers and
   * have yet to be declared as either successful or failed messages
   */
  inflight: number
  /**
   * The amount of messages that are on the delayed queue
   */
  delayed: number
}
