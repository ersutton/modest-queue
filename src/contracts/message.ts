import { Type } from 'class-transformer'
import { MessageMetaData } from './message-metadata'

/**
 * Serializable class that stores the serialized message provided by the client
 * with some meta data that keeps track of retries and other things
 *
 */
export class Message {
  /**
   * The message the client wants to place on the queue
   */
  message: string

  /**
   * The metadata for this message, published date, max retries etc
   */
  @Type(() => MessageMetaData)
  metadata: MessageMetaData

  constructor(message: string, metadata: MessageMetaData) {
    this.message = message
    this.metadata = metadata
  }
}
