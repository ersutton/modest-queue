export enum DelayedStatus {
  /**
   * The polled message is not ready to be placed back on the appropriate queue.
   */
  PolledMessageTooEarly = 'PoppedMessageTooEarly',
  /**
   * No message was found when polling this set
   */
  NoMessageFound = 'NoMessageFound',
  /**
   * The message that was polled has met or exceeded its delayed time.
   * As such it has been placed on the appropriate queue for processing.
   */
  MessagePlacedBackOnQueue = 'MessagePlacedBackOnQueue'
}
