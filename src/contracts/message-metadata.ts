import { Type } from 'class-transformer'

/**
 * Class that stores the metadata in a message
 *
 */
export class MessageMetaData {
  /**
   * A unique uuid for this message.
   * This is required so that all messages, even ones sent with
   * identical serialized data become unique on the queue
   */
  token: string

  /**
   * The date the message was placed n the queue by the client
   */
  @Type(() => Date)
  publishDate: Date

  /**
   * The last date that this message was pulled off the queue to be processed
   * This is mutated on every retry
   */
  @Type(() => Date)
  processedDate: Date | undefined
  /**
   * The configured max attempts for this message
   */
  maxAttempts?: number
  /**
   * The current attempt for this message - this is incremented every time it is pulled from the queue
   */
  currentAttempt: number

  /**
   * how many retries are left before moving to the DLQ - this is set to maxAttempts initially
   * and is decremented every time the message is pulled from the queue
   */
  attemptsLeft?: number

  /**
   * Optional time in milliseconds that the message was delayed
   */
  delay?: number

  /**
   * priority value that was assigned to this message.
   * maximum priority is 100, minimum priority is 0.
   */
  priority: number

  /**
   * The queue score that dictates its position in the overall queue
   */
  score: number

  constructor(
    token: string,
    publishDate: Date,
    currentAttempt: number,
    priority: number,
    score: number,
    delay?: number,
    processedDate?: Date,
    attemptsLeft?: number,
    maxAttempts?: number
  ) {
    this.token = token
    this.publishDate = publishDate
    this.currentAttempt = currentAttempt
    this.priority = priority
    this.delay = delay
    this.processedDate = processedDate
    this.attemptsLeft = attemptsLeft
    this.maxAttempts = maxAttempts
    this.score = score
  }
}
