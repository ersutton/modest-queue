import Redis from 'ioredis'
import { RetryStrategy } from '../retry-strategy'
import { XOR } from './shared/xor'

export type Connection = Redis.Redis

export interface BaseQueueOptions {
  /**
   * The name you wish to call your queue, it will be prepended with `modest-queue` to ensure it is unique
   */
  queueName: string
  /**
   * The time in milliseconds before a message is automatically failed and re-queued after having being popped off the list by a worker. Defaults to 30 seconds (30000)
   * @default 30000 (30 seconds)
   */
  visibilityTimeout?: number
  /**
   * How many times a message can fail before being moved to the Dead Letter Queue
   * @default 10
   */
  maxAttempts?: number
  /**
   * If you would like this queue instance to handle polling for inflight messages, requeuing those that have surpassed the visibility timeout. You should have at least one
   * worker with this set to true.
   * @default true
   */
  withScheduler?: boolean

  /**
   * If you would like this queue instance to promote delayed messages to the queue once the correct time has expired. If you have no need for
   * delayed messages then this can be set to false, if you need delayed messages, you need at lease one worker instance with this set to true.
   * @default true
   */
  withDelayedScheduler?: boolean

  /**
   * Defines how a message retry strategy is to be implemented that calculates the delay between subsequent
   * retries of a message - defaults to the default retry strategy that exponentially increases the delay between retries
   * from 5ms to 2.5 hrs for the first 10 attempts. Each retry delay includes a jitter of
   * up to 10% to avoid deadlock-related errors from continually blocking.
   */
  retryStrategy?: RetryStrategy
}
export interface PassedInConnection extends BaseQueueOptions {
  /**
   * Optional ioredis instance that you pass in. This is useful if you wish to reuse a redis connection between multiple queues and want to save connection overhead.
   * If you do not pass in an ioredis instance, you must supply a @see connectionString
   */
  connection: Connection
}
export interface PassedInConnectionString extends BaseQueueOptions {
  /**
   * The redis connection string to use to connect to the redis instance
   * The redis connection string to use to connect to the redis instance
   * The following is the equivalent of:
   * username: username,
   * password: authpassword
   * host: 127.0.0.1
   * port: 6380
   * db: 4
   * @example redis://username:authpassword@127.0.0.1:6380/4?allowUsernameInURI=true
   *
   * If you do not supply a connection string, you must supply an ioredis instance @see connection
   */
  connectionString: string
}

/**
 * The idea with this type is to let the consumer of this library know, that they can either
 * supply a connection string - in which case this library will handle the creation and destruction
 * of the redis connection using @see initialize and @see dispose. Or you can pass in an ioredis connection
 * that this library will use. In that scenario it is up to you to correctly quit the connection.
 */
export type QueueOptions = XOR<PassedInConnectionString, PassedInConnection>
